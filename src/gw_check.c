#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>

#include "arpping.h"

void get_local_addr(unsigned char *mac, u_int32_t *ip)
{
  struct ifconf interface_conf;
  struct ifreq ifreq1;
  int sock;
  struct sockaddr_in *psockaddr_in = NULL;

  if ((sock = socket(PF_INET, SOCK_DGRAM, 0)) < 0)
    {
      perror("Unable to create socket for getting the mac address");
      exit(1);
    }
  strcpy(ifreq1.ifr_name, "enp3s0");

  if (ioctl(sock, SIOCGIFHWADDR, &ifreq1) < 0)
    {
      perror("Unable to get the mac address");
      exit(1);
    }
  memcpy(mac, ifreq1.ifr_hwaddr.sa_data, 6);
  if (ioctl(sock, SIOCGIFADDR, &ifreq1) < 0)
    {
      perror("Unable to get the ip address");
      exit(1);
    }
  psockaddr_in = (struct sockaddr_in *)&ifreq1.ifr_addr;
  *ip = psockaddr_in->sin_addr.s_addr;
}

int main(int argc, char *argv[])
{
  pid_t pid;
  char gw_ips[16];
  unsigned char local_mac[6], yi_mac[6];
  u_int32_t local_ip;
  struct in_addr gw;
  char interface[] = "enp3s0";
  if (argc == 2)
    {
      strncpy(gw_ips, argv[1], 16);
    }
  else
    {
      printf("Usage: gw_check gateway ip address\n");
      exit(0);
    }
  inet_pton(AF_INET, gw_ips, &gw);
  if (gw.s_addr == 0)
    {
      printf("gateway ip address is invalid, quit\n");
      exit(0);
    }
  /* Demonize */
  pid = fork();
  if (pid > 0) /* parent */
    {
      exit(0);
    }
  else if (pid == 0) /* child */
    {
      /* get interface ip address */
      get_local_addr(local_mac, &local_ip);
      memset(yi_mac, 0, 6);
      while (1)
        {
          if (arpping(gw.s_addr, yi_mac, local_ip, local_mac, interface) != 0)
            {
              printf("Unable to connect gateway\n");
              break;
            }
          sleep(5);
        }
    }
  /* do something while the gateway is disconnected */
  exit(0);
}
