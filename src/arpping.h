#include <netinet/if_ether.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <netinet/in.h>
#define MAC_BCAST_ADDR (uint8_t *)"\xff\xff\xff\xff\xff\xff"

struct arpMsg {
    struct ethhdr ethhdr; /* Ethernet header */
    u_short htype; /* hardware type */
    u_short ptype; /* protocol type */
    u_char hlen; /*hardware address length */
    u_char plen; /*protocol address length */
    u_short operation; /* ARP opcode */
    u_char sHaddr[6]; /* sender's hardware address */
    u_char sInaddr[4]; /* sender's ip address */
    u_char tHaddr[6]; /* target's hardware address */
    u_char tInaddr[4]; /* target's IP address */
    u_char pad[18]; /* pad for min. Ethernet payload (60 bytes) */
};

int arpping(u_int32_t yiaddr, unsigned char *yimac, u_int32_t ip, unsigned char *mac, char *interface);
