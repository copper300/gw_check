#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "arpping.h"

int arpping(u_int32_t yiaddr, unsigned char *yimac, u_int32_t liaddr, unsigned char *mac, char *interface)
{
  int optval = 1;
  int s;
  int rv = -1;
  struct sockaddr addr;
  struct arpMsg arp;
  fd_set fdset;
  struct timeval tm;
  int try = 0;

  if ((s = socket(PF_PACKET, SOCK_PACKET, htons(ETH_P_ARP))) == -1)
    {
      printf("Unable to open raw socket.\n");
      return -1;
    }
  if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval)) == -1)
    {
      printf("Could not setsockopt on raw socket\n");
      close(s);
      return -1;
    }
  /* send arp request */
  memset(&arp, 0, sizeof(arp));
  memcpy(arp.ethhdr.h_dest, MAC_BCAST_ADDR, 6);
  memcpy(arp.ethhdr.h_source, mac, 6);
  arp.ethhdr.h_proto = htons(ETH_P_ARP);
  arp.htype = htons(ARPHRD_ETHER);
  arp.ptype = htons(ETH_P_IP);
  arp.hlen = 6;
  arp.plen = 4;
  arp.operation = htons(ARPOP_REQUEST);
  *((u_int *) arp.sInaddr) = liaddr;
  memcpy(arp.sHaddr, mac, 6);
  *((u_int *)arp.tInaddr) = yiaddr;

  memset(&addr, 0, sizeof(addr));
  strcpy(addr.sa_data, interface);
  for (try = 0; try < 3; try++)
  {
    printf("Before sending \n");
    if (sendto(s, &arp, sizeof(arp), 0, &addr, sizeof(addr)) < 0)
    {
      printf("send error\n");
      rv = -1;
    }
    /* Receive the reply */
    tm.tv_sec = 3;
    tm.tv_usec = 0;
    FD_ZERO(&fdset);
    FD_SET(s, &fdset);

    printf("Before select \n");
    if (select(s + 1, &fdset, (fd_set *)NULL, (fd_set *)NULL, &tm) < 0)
    {
        printf("Error on ARPPING REQUEST: %s", strerror(errno));
        if (errno == EINTR) try = 0;
    }
    else if (FD_ISSET(s, &fdset))
      {
        printf("Receive packet\n");
        if (recv(s, &arp, sizeof(arp), 0) < 0)
          {
            printf("recv error\n");
            rv = -1;
          }
        if (arp.operation == htons(ARPOP_REPLY) && memcmp(arp.tHaddr, mac, 6) == 0 && *((u_int *)arp.sInaddr) == yiaddr)
          {
            printf("Valid arp reply received from this address\n");
            rv = 0;
            if (yimac)
              {
                memcpy(yimac, arp.sHaddr, 6);
              }
            break;
          }
      }
  }
  close(s);
  return rv;
}
